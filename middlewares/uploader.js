const path = require('path');
const multer = require('multer');
// const upload = multer({
//     dest: path.join(process.cwd(), 'uploads')
// })

module.exports = function (filterType) {

    const fileStorage = multer.diskStorage({
        filename: function (req, file, cb) {
            cb(null, Date.now() + '-' + file.originalname);
        },
        destination: function (req, file, cb) {
            cb(null, path.join(process.cwd(), 'uploads/images'));
        }
    })

    function imageFilter(req, file, cb) {
        const fileType = file.mimetype.split('/')[0];
        if (fileType === 'image') {
            cb(null, true)
        } else {
            req.fileTypeError = true;
            cb(null, false)
        }
    }

    function sizeFilter(req, file, cb) {
        const fileSize = file.size
        if (fileSize <= 6000) {
            cb(null, true)
        } else {
            req.fileSizeErr = true;
            cb(null, false)
        }
    }
    const MAP_FILTER = {
        image: imageFilter,
        size: sizeFilter
    }
    // file filter will not block req-res cycle and it will skip the  file upload
    const upload = multer({
        storage: fileStorage,
        fileFilter: MAP_FILTER[filterType]
    })

    return upload;
}
