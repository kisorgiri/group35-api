const http = require('http');
const fileOp = require('./file')

const server = http.createServer(function (req, res) {
    // this callback function is responsible for handling every http client request
    console.log('http client connected to server');
    // console.log('reqis >>', res)
    console.log('req. method >>', req.method);
    console.log('req url >>>', req.url)
    // split the url with /
    // take value from array
    // var action = splittedArr[1];
    // var filename = splittedArr[2];
    // var conten = splittedArr[3]

    // req or 1st argument is for http request object
    // res or 2nd argument is for http response object

    // http client -server communication
    // http method and http url are mandatory
    // regardless of http method and http url this block of code will be executed

    // http request method (http verb)
    // GET,
    // POST
    // PUT
    // DELETE
    // PATCH
    // OPTIONS
    // ...

    // http status code
    // 1--- information
    // 2--- ==> success range
    // 3--- redirectional
    // 4--- ==> application error range
    // 5--- ==> server error

    // endpoint(combination of http method and url) ==> endpoint should be available (ready) for client server communication

    // request response cycle must be completed

    // developer's work
    // to work between req -res cycle


    if (req.url === '/write') {
        fileOp
            .w('js.js', 'I am learning js')
            .then(function (done) {
                res.end('success in writing >>', done);
            })
            .catch(function (err) {
                res.end('error in wriitng >>', err);
            })

    }
    else if (req.url === '/read') {

    } else {
        res.end('no any action to perform')
    }

})

// server is programme
// now run programme into process
server.listen(7777, '127.0.0.1', function (err, done) {
    if (err) {
        console.log('server listeing failed')
    } else {
        console.log('server listening at port 7777')
        console.log('press CTRL + C to exit')
    }
});
