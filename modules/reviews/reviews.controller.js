const ReviewModel = require('./reviews.model');


function get(req, res, next) {
    const condition = {
        product: req.params.productId
    };
    console.log('review fetch condition >>', condition)
    ReviewModel
        .find(condition)
        .populate('user', {
            username: 1
        })
        .exec(function (err, reviews) {
            if (err) {
                return next(err);
            }
            res.json(reviews);
        })

}

function getById(req, res, next) {
    const condition = {
        _id: req.params.id
    };
    ReviewModel
        .findOne(condition)
        .populate('productIduser', {
            username: 1
        })
        .exec(function (err, review) {
            if (err) {
                return next(err);
            }
            res.json(review);
        })
}

function post(req, res, next) {
    // validate data
    // const data = req.body;
    // data.user = req.user._id;

    const newReview = new ReviewModel({});
    newReview.point = req.body.reviewPoint;
    newReview.message = req.body.reviewMessage;
    newReview.user = req.user._id;
    newReview.product = req.params.productId;

    newReview.save(function (err, saved) {
        if (err) {
            return next(err);
        }
        res.json(saved)
    })
}

function update(req, res, next) {

}

function remove(req, res, next) {

}

// object shorthand
module.exports = {
    get: get,
    getById,
    post,
    update,
    remove
}
