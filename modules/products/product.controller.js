const ProductQuery = require('./product.query')

function get(req, res, next) {
    var condition = {};
    if (req.user.role !== 1) {
        // if not admin only fetch product which is added by same user
        condition.vendor = req.user._id;
    }
    ProductQuery
        .find(condition, req.query)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {
    var condition = {
        _id: req.params.id
    }
    ProductQuery
        .find(condition)
        .then(function (data) {
            if (!data[0]) {
                return next({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function post(req, res, next) {
    const data = req.body;
    console.log('req.file >>', req.file)
    console.log('req.files >>', req.files)
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File format",
            status: 400
        })
    }
    // prepare data
    // if(req.file) TODO
    if (req.files) {
        data.images = req.files.map(function (file) {
            return file.filename
        })
    }

    data.vendor = req.user._id;
    ProductQuery
        .insert(data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}

function update(req, res, next) {
    const data = req.body;
    const id = req.params.id;
    console.log('req.body is >>', req.body)

    data.vendor = Object.keys(data.vendor).length
        ? data.vendor._id
        : data.vendor;

    if (req.fileTypeError) {
        return next({
            msg: "Invalid File format",
            status: 400
        })
    }
    if (data.filesToRemove) {
        const intoArr = data.filesToRemove.split(',');
        const image_names_to_remove = intoArr.map(function (file) {
            return file.split('images/')[1]
        })
        data.filesToRemove = image_names_to_remove;
    }
    // remove images so that it will not concatinate on update
    delete data.images;
    // prepare data
    if (req.files.length > 0) {
        const newFilesToUpload = req.files.map(function (file) {
            return file.filename
        })
        data.newFilesToUpload = newFilesToUpload
    }

    ProductQuery
        .update(id, data)
        .then(function (response) {
            // challanges
            // if we have files in request and if db query is successfull
            // remove existing files
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}
function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (removedProduct) {
            if (!removedProduct) {
                return next({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            res.json(removedProduct)
        })
        .catch(function (err) {
            next(err);
        })

}
function search(req, res, next) {
    var searchCondition = {};
    if (req.body.category)
        searchCondition.category = req.body.category;
    if (req.body.name)
        searchCondition.name = req.body.name;
    if (req.body.brand)
        searchCondition.brand = req.body.brand;
    if (req.body.color)
        searchCondition.color = req.body.color;
    if (req.body.minPrice)
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    if (req.body.maxPrice)
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    if (req.body.minPrice && req.body.maxPrice)
        searchCondition.price = {
            $gte: req.body.minPrice,
            $lte: req.body.maxPrice
        }
    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0); // returns milisecods
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);

        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    if (req.body.tags)
        searchCondition.tags = {
            $in: req.body.tags.split(',')
        }

    console.log('search condition >>', searchCondition)

    ProductQuery
        .find(searchCondition) // find is not db(mongoose) method it is our own function
        .then(function (results) {
            res.json(results)
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    get,
    getById,
    post,
    update,
    remove,
    search
}
