const ProductModel = require('./product.model');
const fs = require('fs');
const path = require('path');

function MAP_PRODUCT_REQ_DATA(product, productData) {
    if (productData.name)
        product.name = productData.name;
    if (productData.description)
        product.description = productData.description
    if (productData.brand)
        product.brand = productData.brand
    if (productData.price)
        product.price = productData.price
    if (productData.quantity)
        product.quantity = productData.quantity
    if (productData.quanlity)
        product.quanlity = productData.quanlity
    if (productData.manuDate)
        product.manuDate = productData.manuDate
    if (productData.expirtyDate)
        product.expirtyDate = productData.expirtyDate
    if (productData.salesDate)
        product.salesDate = productData.salesDate
    if (productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate
    if (productData.status)
        product.status = productData.status
    if (productData.isReturnEligible)
        product.isReturnEligible = productData.isReturnEligible
    if (productData.vendor)
        product.vendor = productData.vendor
    if (productData.category)
        product.category = productData.category
    if (productData.size)
        product.size = productData.size
    if (productData.viewsCount)
        product.viewsCount = productData.viewsCount
    if (productData.warrentyStatus)
        product.warrentyStatus = productData.warrentyStatus
    if (productData.warrentyPeroid)
        product.warrentyPeroid = productData.warrentyPeroid
    if (productData.images && productData.images.length)
        product.images = productData.images
    if (!product.discount)
        product.discount = {}
    if (productData.discountedItem)
        product.discount.discountedItem = productData.discountedItem
    if (productData.discountType)
        product.discount.discountType = productData.discountType
    if (productData.discountValue)
        product.discount.discountValue = productData.discountValue
    if (productData.tags)
        product.tags = typeof (productData.tags) === 'string'
            ? productData.tags.split(',')
            : productData.tags;
    if (productData.offers)
        product.offers = typeof (productData.offers) === 'string'
            ? productData.offers.split(',')
            : productData.offers;
    if (productData.colors)
        product.colors = typeof (productData.colors) === 'string'
            ? productData.colors.split(',')
            : productData.colors;
    if (productData.newFilesToUpload && productData.newFilesToUpload.length > 0) {
        product.images.push(...productData.newFilesToUpload)
    }

}

function find(condition, params = {}) {
    const perPage = Number(params.pageSize)
    const currentPage = Number((params.pageNumber || 1) - 1)
    const skipCount = perPage * currentPage;
    return ProductModel
        .find(condition)
        .sort({
            _id: -1
        })
        .limit(perPage || 100)
        .skip(skipCount)
        .populate('vendor', {
            username: 1,
            role: 1
        })
        .exec();
}

function insert(data) {
    // map data
    let newProduct = new ProductModel({});
    MAP_PRODUCT_REQ_DATA(newProduct, data);
    return newProduct.save();
}

function update(id, productData) {
    return new Promise(function (resolve, reject) {

        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            // product found now proceed with update
            MAP_PRODUCT_REQ_DATA(product, productData)
            console.log('files to remove >>', productData.filesToRemove)
            // remove existing files if filesToRemove has values
            if (productData.filesToRemove && productData.filesToRemove.length) {
                product.images.forEach(function (img, index) {
                    productData.filesToRemove.forEach(function (file_name) {
                        if (img === file_name) {
                            product.images.splice(index, 1)
                        }
                    })
                })
            }

            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated)
                if (productData.filesToRemove && productData.filesToRemove.length) {
                    // remove existing images from server
                    productData.filesToRemove.forEach(function (file) {
                        remove_file(file)
                    })
                }
            })
        })
    })
}

function remove(id) {
    return ProductModel.findByIdAndRemove(id)
}

function remove_file(fileName) {
    fs.unlink(path.join(process.cwd(), 'uploads/images/' + fileName), function (err, removed) {
        if (!err) {
            console.log('removed');
        }
    })
}
module.exports = {
    find: find,
    insert: insert,
    update,
    remove
}
