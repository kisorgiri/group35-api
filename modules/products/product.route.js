const router = require('express').Router();
const authenticate = require('./../../middlewares/authenticate');
const authorize = require('./../../middlewares/authorize');
const Uploader = require('./../../middlewares/uploader')('image')

const ProudctCtrl = require('./product.controller');

router.route('/')
    .get(authenticate, ProudctCtrl.get)
    .post(authenticate, Uploader.array('images'), ProudctCtrl.post);

router.route('/search')
    .get(ProudctCtrl.search)
    .post(ProudctCtrl.search);

router.route('/:id')
    .get(authenticate, ProudctCtrl.getById)
    .put(authenticate, Uploader.array('images'), ProudctCtrl.update)
    .delete(authenticate, ProudctCtrl.remove);

module.exports = router;
