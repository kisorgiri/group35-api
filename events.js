const events = require('events');

const myEvent = new events.EventEmitter();
const myEvent1 = new events.EventEmitter();

// trigger
setTimeout(function(){

    myEvent.emit('broadway','hi ');
},2000)

// listen
myEvent1.on('broadway', function (data) {
    console.log('at broadway event >>', data);
})
