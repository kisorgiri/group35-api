const socket = require('socket.io');
const SOCKET_PORT = 9091;
var users = [];
module.exports = function (app) {
    // socket stuff
    const io = socket(app.listen(SOCKET_PORT), {
        cors: '/*'
    });
    io.on('connection', function (client) {
        console.log('client connected to socket server');
        var id = client.id;
        // emit is for client that sents connection request
        //broadcast.emit() // events for other then self client
        // broadcast.to(id).emit(); // selected client only (private)
        client.on('new-user', function (username) {
            users.push({
                id: id,
                name: username
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        client.on('new-message', function (data) {
            client.emit('reply-message-own', data);
            client.broadcast.to(data.receiverId).emit('reply-message-client', data);
        })
        client.on('is-typing', function (data) {
            client.broadcast.to(data.receiverId).emit('is-typing', true)
        })
        client.on('is-typing-stop', function (data) {
            client.broadcast.to(data.receiverId).emit('is-typing', false)

        })

        client.on('disconnect-client', function (username) {
            users.forEach(function (user, index) {
                if (user.name === username) {
                    users.splice(index, 1);
                }
            })
            client.broadcast.emit('users', users);
        })
        client.on('disconnect', function () {
            users.forEach(function (user, index) {
                if (user.id === id) {
                    users.splice(index, 1);
                }
            })
            client.broadcast.emit('users', users);
        })
    })
}
