const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CartSchema = mongoose.Schema({
    // db modelling
    product: {
        type: Schema.Types.ObjectId,
        ref: 'proudct'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    status: ['draft', 'purchased', 'cancelled']
}, {
    timestamps: true
})

const CartModel = mongoose.model('cart', CartSchema)
module.exports = CartModel;
