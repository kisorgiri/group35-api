// PL ====> Server Side PL
// ==> database communication, Network Level Application, socket communication, API, Web Server
// web server (nginx, apachhe, tomcat, ...) nodejs
// CL ====> Client Side PL
// UI/UX

// NODEJS ===> framework/library haina

// run time environment 
// server side JS ===> NODEJS


// Scripting language ==> can be embedded in other programming language
// programming language ==>


// 1995 ==> JS was introduced
// 2009 ==> Client Side 
// 2021 ==> Server Side as well Client Side


// 2009 ==> ryan Dahl introduced NODEJS
// NODEJS is Servers SIDE RUN TIME ENVIRONMENT

// application area for server side

// ==> database communication, Network Level Application, socket communication, API, Web Server

// Goal ===> to build web application
// web server  ==>nodejs & express
// web client  == react




// command

// node --version 
// npm --version
// npm is developement tool for pacakge management
// npx is also pacakge management tool
// npx will be covered when learning react

// start JS project
//regardless of Application area we use nodejs
// nodejs is used for client atleast for development process


// package.json ==> package.json is project's introductory file
// try to keep updated and correct information inside pacakge.json
// package-lock.json ===> locks exact version of dependent packages(dependency tree ma aaune packages)  
// version will be latest version of packages during first installation
// node_modules==> container that holds the modules installed via npm install command (yarn add)

// TODO FILE _FILE COMMUNICATION
// // to perform file-file communication 
// one file must import
// one file must export

// export syntax
// es5 syntax
// module.exports = <any value>

// es6 export 
// two ways of export
// export
// export default

// import syntax
// es5
// const imported_name = require('path to your js file')

//es6 syntax
// import depends on how it is exported

// import {any_name} from 'location of file';
// import name from 'location of file'

// IMP 
// to import nodejs internal module 
// and node_modules pacakges(modules)
// we should not give path when importing

// const a = require('events')\

// Objective
// to create a web application

// web architecture
// protocol ==> set of rules for communication between two programme
// smtp,ftp,http

// web ko synonym is http
// web server === http server
// web client ==== http client

// investigate http protocol ===> status code , method , request, response

// Client-Server Architecture
// ==>programmes can be a client and server
// client programme is responsible for initiating request
// server programme must be ready(listening) for accepting client's request
// server programme is responsible for handling request and completing response
// tier architecture
// data storage
// views 
// controllers
// 3-tier ==> presentation layer, application layer, data layer
// web architecture (MVC)
// views, contoller, models
// var abcd;
// console.log('check vlaue >>', abcd.ticket);


// application ==> e commerce
// Entities list
// users, products,reviews,notifications,offers
// attributes
// db modelling


// product CRUD
// 

// notification CRUD,Payment, cart  CRUD
//  es6 videos
// es6 express starter kit
// https://github.com/developit/express-es6-rest-api


// server side
// database backup and restore
// REST API
// TODO
// entities CRUD, notification, payment_history, cart
// Event Based Communication Socket Communication (real time notification)
// Aggregation ==> 
// Email sending
// deployment

// --->  REST
// ---> API

// API (application programming interface)
// interface ===> endpoint
// EndPoint ===> Combination of http method and URL
// eg. POST /auth/login, PUT /user/id

// REST (Representation State Transfer)
// architecture
// following points needs to be fullfilled to be on REST
// 1. stateless communication
// server will never know what was previous request
// server will not save any client request
// 2. correct use of http verb
// GET ==> data fetch
// POST ==> data send
// PUT /PATCH===> Update
// DELETE ==> Remove
// 3. data format must be in either XML or JSON
// JSON everywhere
// 4. cache GET request (to prevent frequent db call) (optional)


// REST API
// REST Architecture follow garne API


// ready for front end
// basic overview
// html basics, css basics
// task runners ==> gulp grunt >> webpack
// compiler ==> babel
// JSX
