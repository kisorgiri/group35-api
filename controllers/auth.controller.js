const express = require('express');
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader')('image')
const passwordHash = require('password-hash')
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const randomStr = require('randomstring');

const sender = require('./../configs/email.configs');

function prepareEmail(data) {
    return {
        from: 'Group 35 Application', // sender address
        to: "shakyadipen@gmail.com,nirajanshrestha911@gmail.com,Rajbhattalok42@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<p>Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system please use link below to reset your password.
        We noticed that you are having trouble logging into our system please use link below to reset your password.</p>
        <p><a href="${data.link}" >Click here to reset password</a></p>
        <p>Regards,</p>
        <p>Group 35 Support Team</p>`, // html body
    }
}

function generateToken(data) {
    var token = jwt.sign({
        _id: data._id,
        role: data.role,
        name: data.username
    }, config.JWT_SECRET)
    return token;
}

const router = express.Router(); // router is routing level middleware

router.get('/', function (req, res, next) {
    require('fs').readFile('lkdsf.sdlkjf', function (err, done) {
        if (err) {
            req.myEvent.emit('error', err, res)
        }
    })
})

router.post('/login', function (req, res, next) {
    UserModel
        .findOne({
            $or: [
                { username: req.body.username },
                { "contactInformation.email": req.body.username }
            ]
        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Invalid Username",
                    status: 400
                })
            }
            var isMatched = passwordHash.verify(req.body.password, user.password);
            if (!isMatched) {
                return next({
                    msg: "Invalid Password",
                    status: 400
                })
            }

            if (user.status !== 'active') {
                return next({
                    msg: 'Your account is disabled please contact IT Department for support',
                    status: 400
                })
            }

            // if user
            // check status
            // password verfication
            // token generation
            var token = generateToken(user);
            res.json({
                user: user,
                token: token
            });
        })
        .catch(function (err) {
            next(err)
        })

})

router.post('/register', uploader.single('image'), function (req, res, next) {

    // Field Validation
    if (!req.body.password || !req.body.username) {
        return next({
            msg: 'missing required fields',
            status: 400
        })
    }
    const data = req.body;
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File format",
            status: 400
        })
    }
    console.log('req.body >>', req.body)
    // prepare data 
    if (req.file) {
        // var fileType = req.file.mimetype.split('/')[0];
        // if (fileType !== 'image') {
        //     return next({
        //         msg: 'Invalid file format',
        //         status: 400
        //     })
        //     // remove uploaded file
        // }
        data.image = req.file.filename
    }

    const newUser = new UserModel({});
    // newUser is a mongoose object
    const newMappedUser = MAP_USER_REQ(newUser, data);
    newMappedUser.password = passwordHash.generate(req.body.password);
    // useUser is mongoose object so call mongoose method for db operation
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done);
    })



    // check form data
    // validation  TODO
    // db_operation
    // steps for db operation
    // 1. open connection
    // 2. select database
    // 3. perform operation
    // 4. db connection close();
})

router.post('/forgot-password', function (req, res, next) {
    const email = req.body.email;

    UserModel.findOne({
        "contactInformation.email": email
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "No accounts linked with provided email",
                status: 400
            })
        }
        var resetToken = randomStr.generate(28);
        var passwordResetExpiryTime = Date.now() + (1000 * 60 * 60 * 24);
        // user found now proceed email
        var emailData = {
            name: user.username,
            email: user.contactInformation.email,
            link: `${req.headers.origin}/reset_password/${resetToken}`
        }
        const emailBody = prepareEmail(emailData);
        user.passwordResetExpiryTime = passwordResetExpiryTime;
        user.passwordResetToken = resetToken;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            sender.sendMail(emailBody, function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })

    })
})

router.post('/reset-password/:token', function (req, res, next) {
    UserModel.findOne({
        passwordResetToken: req.params.token,
        passwordResetExpiryTime: {
            $gte: Date.now()
        }
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "Reset Password Token Invalid/Expired",
                status: 400
            })
        }
        // if (new Date(user.passwordResetExpiryTime).getTime() <= Date.now()) {
        //     return next({
        //         msg: "Reset Password Token expired",
        //         status: 400
        //     })
        // }
        user.password = passwordHash.generate(req.body.password);
        user.passwordResetExpiryTime = null;
        user.passwordResetToken = null;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            res.json(saved);
        })
    })
})




module.exports = router;
