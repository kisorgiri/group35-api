const router = require('express').Router();
const UserModel = require('./../models/user.model')
const MAP_USER_REQ = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader')('image')

router.route('/')
    .get(function (req, res, next) {
        var condition = {};
        // projection {
        //     username:1,
        //     'contactInformation.phoneNumber':1
        // }
        UserModel
            .find(condition)
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users);
            })
    })
    .post(function (req, res, next) {
        // application level middleware
    });

router.route('/search')
    .get(function (req, res, next) {
        res.send('from user search ')
        // application level middleware
    })
    .post(function (req, res, next) {
        // application level middleware
    });



router.route('/:id')
    .get(function (req, res, next) {
        var id = req.params.id;
        UserModel.findById(id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User Not Found',
                    status: 404
                })
            }
            res.json(user);
        })
    })
    .put(uploader.single('image'), function (req, res, next) {
        const id = req.params.id;
        const data = req.body;
        if (req.fileTypeError) {
            return next({
                msg: "Invalid File Format",
                status: 400
            })
        }

        if (req.file) {
            data.image = req.file.filename
        }
        UserModel.findById(id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User Not Found',
                    status: 404
                })
            }
            // user found now proceed with update
            // user is mongoose object
            const updatedUser = MAP_USER_REQ(user, data)

            // temp
            console.log('req.user is >>', req.user)
            user.updatedBy = req.user.username;
            // temp
            user.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                // MUST TODO 
                // be aware of mutable behaviour
                // remove exiting old image once updated with new image
                res.json(updated)
            })

        })
    })
    .delete(function (req, res, next) {
        console.log('req.user >>', req.user);
        if (req.user.role !== 1) {
            return next({
                msg: 'Authorization Failed,You Don`t have accesss',
                status: 403
            })
        }
        var id = req.params.id;
        UserModel.findById(id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed)
            })
        })
    });

module.exports = router;
