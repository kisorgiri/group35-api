const express = require('express');
const config = require('./configs');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');

const app = express();
// app is entire express framework

// socket stuff
require('./socket.js')(app)
// event driven communication
const events = require('events');
const myEvent = new events.EventEmitter();
myEvent.on('error', function (err, res) {
    console.log('att error event handler', err);
    res.json(err);
})

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

require('./db_init');

// import middlewares
const NotFound = require('./middlewares/notFound')

// import main routing file
const API_ROUTE = require('./routes/api.route');

// load third party middleware
app.use(morgan('dev'))
app.use(cors()) // accept all incoming request

// load inbuilt middleware
app.use(express.static('uploads/images')) // internal serve
app.use('/file', express.static(path.join(process.cwd(), 'uploads'))) // external server
// so all incoming request with data will have content-type in the req headers
// server must parse all those incoming data accordance with content-type
// parser of x-www-form-url encoded
app.use(express.urlencoded({
    extended: true
}))
// json parser
app.use(express.json())

// parse incoming data and add those data as key value pair inside request's body
// load routing level middleware
// mount incoming request
app.use('/api', API_ROUTE)

// handle 404 errors
app.use(NotFound)

// middleware with 4 arguments is error handling middleware
// error handling middleware must be called
// to call error handling middleware we use next with argument
// next(any value)
app.use(function (err, req, res, next) {
    console.log('error is >>', err)
    // err or 1st argument is place holder for errors
    // req,res,next is similar to application level middlware
    res.status(err.status || 400);
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

app.listen(process.env.PORT || config.PORT, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + config.PORT);
        console.log('press CTRL + C to exit')
    }
})


// / ENDPOINT SPECIFIC HANDLER
// // method get and url /
// app.get('/', function (req, res) {
//     // req res cycle must be completed
//     res.send('from empty url of get')
    // res.sendStatus(400);
    // req-res cycle must be completed with data as a response
    // res.json
    // res.send
    // res.sendFile
    // res.download

    // express as an independant web application
    // res.redirect
    // res.render

    // res.status ==> it will not end the cycle but sets status code for response

    // response must be sent only once for a request

// })

// core topic ==> MIDDLEWARE

// middleware is a function which has access to http request object http response object and next middleware function reference
// middleware alwyas came into action in between request response cycle
// middleware can access and modify http req- http res object

// IMP the order of middleware matters

// syntax
// function (req,res,next){
    // req or 1st argument is http request object
    // res  or 2nd argument is http response object
    // next or 3rd argument is next middleware function reference
// }


// configuration block
// .use, .all, .get,.post,.delete 
// app.use(function(req,res,next){
// app.use is configuration for middleware
// })


// types of middleware
// 1.Application Level Middleware
// application level middleware has direct scope(acess) of http req ,res and next

// 2.routing middleware
// 3.thirdparty middleware
// 4.inbuilt middleware
// 5. error handling middleware
