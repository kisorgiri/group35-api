const router = require('express').Router();
// routing configuration
// load middlewares
const Authenticate = require('./../middlewares/authenticate')
const Authorize = require('./../middlewares/authorize')

// load routing level middleware
const AuthRouter = require('./../controllers/auth.controller')
const UserRouter = require('./../controllers/users.controller')
const ReviewRouter = require('./../modules/reviews/reviews.route');
const ProductRouter = require('./../modules/products/product.route');


router.use('/auth', AuthRouter)
router.use('/user', Authenticate, UserRouter)
router.use('/review', Authenticate, ReviewRouter)
router.use('/product', ProductRouter)

module.exports = router;
