const mongodb = require('mongodb');
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'group35db';
// we need mongoCLient
const MongoClient = mongodb.MongoClient;
const OID = mongodb.ObjectID;

module.exports = {
    conxnURL, //es6 object shorthand
    dbName,
    MongoClient,
    OID
}
