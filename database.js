// database is a container
// CRUD operation
// Create Read Update Delete

// DBMS (Database Management System) is used to manipulate data in a database

// two type DBMS
// 1. Relational DBMS
// 2. Non-Relational DBMS (Document based database)

// 1. Relational DBMS
// a.Table based Design
// eg entities ===> Users, Books,Notifications
// b. Schema based design
//====> Entities with their attributes(properties that define entities) are designed in a way that arecord must validate the schema
// c. each record are called row, tuple
// each row should validate the provided schema
// d. non scalable
// e. relation between table exists
// f. SQL database (Structured Query Languages)
// g. mysql, postgres, sql-lite,

// eg Books , Comments ==> 
// {name:'dsa',genre:'programming',id:2}, comments ={message:'good one,book_id:2,userId,3}

// 2. Non Relational Database 
// a.COllection Based Desgin
// entities ==> User, Books, Notifications
// b. Schema less solution
// c.each record are called document
// document can be a valid JSON
// d. higly scalable 
// e. relation doesnot exists 
// f. NO SQL, Not Only SQL
// g. mongodb, couchdb, dynamodb,redis,

// books
var abook = {
    name: 'design',
    description: 'good one',
    reviews: [{
        user: {
            username: 'test',
            role: 'admin'
        },
        point: 3,
        message: 'good one'
    },
    {
        user: {
            username: 'test',
            role: 'admin'
        },
        point: 3,
        message: 'good one'
    }]
}


// mongodb setup in windows
// download
// install (compass uncheck [fast installation])
// c://programefiles>mongodb
// mongodb/version/bin/ executable file
// my computer right click ===> properties, ===> advanced_settings===> environment variables ===> system variables
// ===>path ==> new ===> paste the bin directory copied path
// powershell


// mongo shell

// client -server communication
// mongodb://localhost:27017 --> server is listening

// client 
// mongo shell (client 1)
// UI tool (client 2)
// application built in programming language

// mongo shell
// to connect with mongo shell type mongo in any location
// it will provide > interface to accept shell commands
// shell command
// db operation (CRUD)

// show dbs ===> list all the available databases

// use <db_name> [if db exists reselect it , else create new database and select]
// db ===> selected db

// show collections ===> list all the available collections from selected db

// C(create)
// db.<collection_name>.insert({valid json})
// db.<collection_name>.insertMany({valid json})
// if collection exist it will add documents to existing collection else create new collection and add documents
// eg db.users.insert({prop and value pair});
// mongodb will add _id property with value as objectId for each document


// R(read)
// db.<collection_name>.find({query_builder})
// db.<collection_name>.find({query_builder}).pretty() // shows formatted view
// db.<collection_name>.find({query_builder}).count() // returns count of collection fullfilling given condition
// db.<collection_name>.find({criteria}).sort(order)
// db.<collection_name>.find({condition}).limit(limitCount)
// db.<collection_name>.find({condition}).skip(.skipCount)

// projection
// either inclusion or exclusion of property when running find query
// db.<collection_name>.find({query_builder},{
    // brand:1 //inclusion
    // price:0 // exculsion
    // NOTE:- either inclusion or exclusion is used
// })

// range 
// operator $gt,$gte, $lt,$lte ==> greater then , greater then equals
// less than and less then equals

// array search
// $in $all
// $in return  document that matches any items specified inside $in
// $all retuns document that matches every items specified in provided array


// U (update)
// db.<collection_name>.update({},{},{});
// 1st object ==> query builder
// 2nd object ==> will have key as $set and value as another object to be updated
// 3rd object ==> optional ==> options

// db.laptops.update({_id:ObjectId('hex code ')},{$set:{object to be updated}},{options})
// db.laptops.update({color:'black'},{$set:{object to be updated}},{multi:true})

// D(remove)
// db.laptops.remove({condition})
// NOTE :-- do not leave the condition empty 

// drop collection
// db.<collection_name>.drop();

// drop database 
// db.dropDatabase();


// ORM || ODM
// ORM ==> relational database  ==> Object Relation Mapping 
// ODM ==> document based database ==> Object Document Modelling 
// database mongodb server side ==> nodejs =======> mongoose
// mongoose is our ODM

// advantages of using mongoose
// 1. Schema defination
// -----> database modelling ===> initial stage of application building ===>collect data and plan to convert it to information
// 2. Data type 
// ----> type restriction for attributes of a schema eg Date, String, Number,Array...
// 3. methods ==> functions performing database operation ,eg findById, find,findOne, save, .....
// 4.middleware ===> pre 
// 5.indexing are lot more easier ==> unique, required

//######################## DB BACKUP AND RESTORE#####################
// 1.bson format 2. json and csv
// command
// mongodump and mongorestore
// mongoimport and mongoexport

// 1.bson format
// backup
// command ==> mongodump
// mongodump ==> it will backup all the available database in default dump folder
// mongodump --db <db_name> it will backup selected database inside dump folder
// mongodump --db <db_name> --out <path_to_destination_folder> creates folder to keep the bson data

// restore 
// command ===> mongorestore
// mongorestore ===> it will restore all the database available inside dump folder
// mongorestore --drop ==> it drops existing document and restore from backup folder
// mongorestore --drop <path_to_source_folder_containing_bson data>

// 2. json and csv
// 2.1 json
// backup
// command  ==> mongoexport
// mongoexport --db <db_name> --collection<collection_name> --out <path_to_destination_folder_with.json extension>
// restore
// command  ===> mongoimport
// mongoimport --db <db_name>[new or existing db] --collection <new or existing collection name> <path_to_ json_source_file>
// mongoimport -d <db_name>[new or existing db] -c <new or existing collection name> <path_to_ json_source_file>

// 2.2 CSV
// backup command ==> mongoexport
// restore command -===> mongoimport
// backup
// mongoexport --db <db_name> --collection <collection_name> --type=csv --fields <comma,seperated fields to be exported> --out <path_to_destination_folder with .csv extension>
// mongoexport --db <db_name> --collection <collection_name> --type=csv --query="{'key':'value'}" --fields <comma,seperated fields to be exported> --out <path_to_destination_folder with .csv extension>

// restore
// mongoimport --db <existing or new db> --collection <existing or new collection> --type=csv <path_to_csv_file> --headerline

//######################## DB BACKUP AND RESTORE#####################
