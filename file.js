const fs = require('fs');

// write file

// fs.writeFile('./files/k.txt', 'welcome to nodejs and server side', function (err, done) {
//     if (err) {
//         console.log('error in writing file', err);
//     } else {
//         console.log('success in write file', done);
//     }
// });

// readfile
// fs.readFile('./files/broadway.txt', 'UTF-8', function (err, done) {
//     if (err) {
//         console.log('error in read', err);
//     } else {
//         console.log('success in read >', done)
//     }
// })

// // rename
// fs.rename('./files/abc.js', './files/xyz.js', function (err, done) {
//     if (err) {
//         console.log('error in rename', err);
//     } else {
//         console.log('success in rename >>', done);
//     }
// })

// // remove
// fs.unlink('./files/k.txt', function (err, done) {
//     if (err) {
//         console.log('error in removing',err);
//     } else {
//         console.log('success in removing >>', done)
//     }
// })

// task
// 1. make it functional
// 2. handle result[either callback or promise]
// 3. export and run from another file by importing [result should be handled]

function myWrite(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + fileName, content, function (err, done) {
            if (err) {
                reject(err)
            } else {
                resolve(done)
            }
        })

    })

}

// myWrite('broadway55.txt', 'welcome to Brodway')
//     .then(function (data) {
//         console.log('data is >>', data);
//     })
//     .catch(function (err) {
//         console.log('eror r is >>', err);
//     })


module.exports = {
    w: myWrite
}
